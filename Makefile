#change the CCFLAGS and USRLIB to MRMPI library paths.
CC =            mpicc
CPP =           mpic++
CCFLAGS =       -g -O -I /home/armen.abnousi/mrmpi-7Apr14/src -std=c++0x
LINK =          mpic++
LINKFLAGS =     -g -O
USRLIB =        /home/armen.abnousi/mrmpi-7Apr14/src/libmrmpi_mpicc.a
SYSLIB =        -lpthread


# Targets

all: kmer_clustering


kmer_clustering:	profiler.o $(USRLIB)
	$(LINK) $(LINKFLAGS) profiler.o $(USRLIB) $(SYSLIB) -o profiler

clean:
	rm *.o profiler

# Rules

%.o:%.cpp
	$(CPP) $(CCFLAGS) -c $<

%.o:%.c
	$(CC) $(CCFLAGS) -c $<
