import sys

def main():
	freq_dataset_filename = sys.argv[1]
	print(freq_dataset_filename)
	pfam_results = sys.argv[2]
	print(pfam_results)
	output_filename = sys.argv[3]
	threshold = int(float(sys.argv[4]))
	kmer_size = int(float(sys.argv[5]))
	more_doms = kmer_size - 1
	freqs = read_freq_dataset(freq_dataset_filename)
	regions, dom_counts = read_pfam_results(pfam_results)
	for dom in dom_counts:
		print(dom + ' appeared ' + str(dom_counts[dom]) + ' times')
	generate_dataset(freqs, more_doms, regions, dom_counts, output_filename, threshold)

def read_freq_dataset(filename):
	with open(filename) as infile:
		lines = infile.readlines()
	name = ''
	freqs = {}
	last_line_read = 'freqs'
	for line in lines:
		if line[0] == '>':
			if name != '':
				freqs[name] = freq
				if last_line_read != 'freqs':
					print('error while reading name')
					exit(1)
			name = line[ 1 : -1]
			last_line_read = 'name'
		else:
			if last_line_read == 'name':
				freq = line[ : -1]
				freq = freq.split('\t')
				last_line_read = 'freqs'
			else:
				print('error while reading freq')
				exit(1)
	freqs[name] = freq
	return(freqs)

def read_pfam_results(filename):
	with open(filename) as infile:
		lines = infile.readlines()
	regions = {}
	dom_counts = {}
	for line in lines:
		line = line[ : -1]
		line = line.split('\t')
		name = line[0]
		pos = ( (int(float(line[3])) - 1), (int(float(line[4])) - 1) )
		dom_name = line[5]
		if dom_name in dom_counts:
			dom_counts[dom_name] += 1
		else:
			dom_counts[dom_name] = 1
		if name not in regions:
			regions[name] = []
		regions[name].append((dom_name, pos))
	return(regions, dom_counts)

def generate_dataset(freqs, more_doms, regions, dom_counts, filename, threshold):
	output = ""
	for name, freq in freqs.items():
		dom = ['0'] * (len(freq) + more_doms)
		if name not in regions:
			print("no region found for: " + name)
		else:
			for (dom_name, (start, end)) in regions[name]:
				if dom_counts[dom_name] > threshold:
					for index in range(start, end + 1):
						dom[index] = '1'
		output += ('>' + name + '\n' + '\t'.join(freq) + '\n' + '\t'.join(dom) + '\n')
	with open(filename, 'w') as outfile:
		outfile.write(output)

if __name__ == '__main__':
	main()
