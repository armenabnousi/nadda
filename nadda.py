import sys, getopt, csv, random, time
from sklearn import metrics
import numpy
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier
import os.path
import pickle

def main(argv):
	try:
		opts, args = getopt.getopt(argv,"hi:t:m:v:w:M:k:")
	except getopt.GetoptError:
		print_instructions()
		sys.exit(2)
	train_filename = ""
	model_filename = ""
	mss = 100
	win_size = 10
	max_features = 7
	kmer_size = 6
	for opt, arg in opts:
		if opt == '-h':
			print_instructions()
			sys.exit(0)
		elif opt == "-i":
			train_filename = arg
		elif opt == "-t":
			test_filename = arg
		elif opt == "-m":
			mss = int(float(arg))
		elif opt == "-v":
			max_features = int(float(arg))
		elif opt == "-w":
			win_size = int(float(arg))
		elif opt == "-M":
			model_filename = arg
		elif opt == "-k":
			kmer_size = int(float(arg))
	o = DecisionTreeClassifier(criterion = 'entropy', min_samples_split = mss)
	o1 = BaggingClassifier(base_estimator = o, max_features = max_features)
	if train_filename != "":
		if model_filename == "":
			model_filename = train_filename + ".model"
		print("Reading training file and generating instances...")
		sys.stdout.flush()
		train_X, train_y = train_data_generator(train_filename, win_size)
		print("Training the classifier...")
		sys.stdout.flush()
		o1.fit(train_X, train_y)
		print("model trained!")
		sys.stdout.flush()
		with open(model_filename, "wb") as modelfile:
			pickle.dump(o1, modelfile)
		print("model written to file!")
		sys.stdout.flush()
	else:
		if model_filename == "":
			model_filename = "model.default"
		if not os.path.isfile(model_filename):
			print("You should either specify a training dataset or an existing model file!")
			exit(1)
		else:
			print("loading model: " + model_filename)
			with open(model_filename, 'rb') as pickled_file:
				o1 = pickle.load(pickled_file)
	print("reading test data...")
	sys.stdout.flush()
	test_X, test_seq_names = test_data_generator(test_filename, win_size, kmer_size)
	print("model parameters: ")
	print(o1.get_params())
	scores_validation = []
	scores_train = []
	print("Predicting...")
	sys.stdout.flush()
	pred_test = o1.predict(test_X)
	detected_domain_printer(test_X, pred_test, test_seq_names)

def print_instructions():
	print('nadda.py -i <train_file> -t <test_file> -m <mss> -v <max_features> -w <win_size> -M <model_file> -k <kmer_size>')

def detected_domain_printer(X, preds, test_seq_names):
	seq_last_poses = indexer(X)
	numpy.set_printoptions(threshold = numpy.nan)
	correct_guesses = 0
	wrong_guesses = 0
	more_preds = 0 
	more_counter = 0
	less_preds = 0
	less_counter = 0
	begin = 0
	end = seq_last_poses[0]
	counter = 0
	for end in seq_last_poses:
		print("***1 indicates a predicted conserved index and -1 indicates a predicted non-conserved index!***")
		print('>' + test_seq_names[counter] , "\n" , "predictions: " , preds[begin : (end + 1)] , "\n")
		begin = end + 1
		counter += 1

def indexer(X):
	last_poses = []
	for i in range(1, X.shape[0]):
		if X[i][1] == -2 and X[i][0] != -2:
			is_new_seq = True
			last_poses.append(i - 1)
	last_poses.append(X.shape[0] - 1)
	return last_poses

def train_data_generator(train_filename, win_size):
	before_dom = 0
	after_dom = 0
	seq_names = []
	seq_pos_freqs = []
	seq_domain_pos = []
	(seq_names, seq_pos_freqs, seq_domain_pos, seq2length, total_length) = read_train_dataset(train_filename)
	train_X, train_y, test_X, test_y = make_matrix(seq_names, seq_pos_freqs, seq_domain_pos, win_size, win_size, before_dom, after_dom, [], total_length, 0)
	return train_X, train_y

def test_data_generator(test_filename, win_size, kmer_size):
	before_dom = 0
	after_dom = 0
	seq_names = []
	seq_pos_freqs = []
	seq_domain_pos = []
	seq2length = {}
	total_length = 0
	(test_seq_names, test_seq_pos_freqs, test_seq2length, test_total_length) = read_test_dataset(test_filename, kmer_size)
	seq_names.extend(test_seq_names)
	seq_pos_freqs.extend(test_seq_pos_freqs)
	seq2length.update(test_seq2length)
	total_length += test_total_length
	test_X = make_test_matrix(seq_names, seq_pos_freqs, win_size, win_size, test_seq_names, total_length, kmer_size)
	return test_X, test_seq_names

def make_test_matrix(names, db_freqs, before_freq, after_freq, test_names, total_test_length, kmer_size):
	train_instances = []
	train_15m_instances = []
	test_instances = []
	test_seq_names = []
	test_seq_lengths = []
	counter = 0
	bigcounter = 0
	test_matrix = numpy.empty((total_test_length, 1 + before_freq + after_freq))
	test_matrix.fill(-2)
	testseq2length = {}
	test_counter = 0
	train_counter = 0
	ready_tests = []
	for seq_number in range(len(db_freqs)):
		if names[seq_number] in test_names:
			if names[seq_number] not in ready_tests:
				is_test_instance = True
				ready_tests.append(names[seq_number])
			else:
				is_test_instance = False
		else:
			is_test_instance = False
		for i in range(len(db_freqs[seq_number]) + kmer_size - 1):
			instance = []
			counter += 1
			feature = 1
			for before_i_freq in range(min(i, len(db_freqs[seq_number]) - 1), max(0, i - before_freq) - 1, -1):
				if (len(db_freqs[seq_number]) <= i and feature == 1):
					feature += (before_freq - len(range(min(i, len(db_freqs[seq_number]) - 1), max(0, i - before_freq) - 1, -1)) + 1)
				if is_test_instance:
					test_matrix[test_counter, feature - 1] = db_freqs[seq_number][before_i_freq]
				else:
					train_matrix[train_counter, feature - 1] = db_freqs[seq_number][before_i_freq]
				feature += 1
			feature = before_freq + 1 + 1
			for after_i_freq in range( i + 1, min(len(db_freqs[seq_number]), i + after_freq + 1), 1):
				if is_test_instance:
					test_matrix[test_counter, feature - 1] = db_freqs[seq_number][after_i_freq]
				else:
					train_matrix[train_counter, feature - 1] = db_freqs[seq_number][after_i_freq]
				feature += 1
			feature = before_freq + 1 + after_freq + 1
			if is_test_instance:
				test_counter += 1
			else:
				train_counter += 1
	return (test_matrix)

def make_matrix(names, db_freqs, db_doms, before_freq, after_freq, before_dom, after_dom, test_names, total_length, total_test_length):
	train_instances = []
	train_15m_instances = []
	test_instances = []
	test_seq_names = []
	test_seq_lengths = []
	counter = 0
	bigcounter = 0
	train_matrix = numpy.empty((total_length - total_test_length, 1 + before_freq + after_freq + before_dom + after_dom)) 
	train_matrix.fill(-2)
	train_y = numpy.empty(total_length - total_test_length)
	test_matrix = numpy.empty((total_test_length, 1 + before_freq + after_freq + before_dom + after_dom))
	test_matrix.fill(-2)
	test_y = numpy.empty(total_test_length)
	testseq2length = {}
	test_counter = 0
	train_counter = 0
	ready_tests = []
	for seq_number in range(len(db_doms)):
		bigcounter += len(db_doms)
		if names[seq_number] in test_names:
			if names[seq_number] not in ready_tests:
				is_test_instance = True
				ready_tests.append(names[seq_number])
			else:
				is_test_instance = False
		else:
			is_test_instance = False
		for i in range(len(db_doms[seq_number])):
			instance = []
			counter += 1
			if db_doms[seq_number][i] == "0" or db_doms[seq_number][i] == "0\n":
				if is_test_instance:
					test_y[test_counter] = -1
				else:
					train_y[train_counter] = -1
			else:
				if is_test_instance:
					test_y[test_counter] = +1
				else:
					train_y[train_counter] = +1
			feature = 1
			for before_i_freq in range(min(i, len(db_freqs[seq_number]) - 1), max(0, i - before_freq) - 1, -1):
				if (len(db_freqs[seq_number]) <= i and feature == 1):
					feature += (before_freq - len(range(min(i, len(db_freqs[seq_number]) - 1), max(0, i - before_freq) - 1, -1)) + 1)
				if is_test_instance:
					test_matrix[test_counter, feature - 1] = db_freqs[seq_number][before_i_freq]
				else:
					train_matrix[train_counter, feature - 1] = db_freqs[seq_number][before_i_freq]
				feature += 1
			feature = before_freq + 1 + 1
			for after_i_freq in range( i + 1, min(len(db_freqs[seq_number]), i + after_freq + 1), 1):
				if is_test_instance:
					test_matrix[test_counter, feature - 1] = db_freqs[seq_number][after_i_freq]
				else:
					train_matrix[train_counter, feature - 1] = db_freqs[seq_number][after_i_freq]
				feature += 1
			feature = before_freq + 1 + after_freq + 1
			for before_i_dom in range(i-1, max(0, i - before_dom) - 1, -1):
				if db_doms[seq_number][before_i_dom] == '0' or db_doms[seq_number][before_i_dom] == '0\n':
					if is_test_instance:
						test_matrix[test_counter, feature - 1] = -1
					else:
						train_matrix[train_counter, feature - 1] = -1
				if db_doms[seq_number][before_i_dom] == '1' or db_doms[seq_number][before_i_dom] == '1\n':
					if is_test_instance:
						test_matrix[test_counter, feature - 1] = +1
					else:
						train_matrix[train_counter, feature - 1] = +1
				feature += 1
			feature = before_freq + 1 + after_freq + before_dom + 1
			for after_i_dom in range(i + 1, min(len(db_doms[seq_number]), i + after_dom + 1), 1):
				if db_doms[seq_number][after_i_dom] == '0' or db_doms[seq_number][after_i_dom] == '0\n':
					if is_test_instance:
						test_matrix[test_counter, feature - 1] = -1
					else:
						train_matrix[train_counter, feature - 1] = -1
				if db_doms[seq_number][after_i_dom] == '1' or db_doms[seq_number][after_i_dom] == '1\n':
					if is_test_instance:
						test_matrix[test_counter, feature - 1] = 1
					else:
						train_matrix[train_counter, feature - 1] = 1
				feature += 1
			if is_test_instance:
				test_counter += 1
			else:
				train_counter += 1
	return train_matrix, train_y, test_matrix, test_y

def read_train_dataset(filename): 
	names = []
	freqs = []
	doms = []
	seq2length = {}
	total = 0
	print(filename)
	with (open(filename)) as datafile:
		lines = datafile.readlines()
	type_var = 0
	for row in lines:
		if row[0] == '>':
			names.append(row[1:-1])
			if type_var != 0:
				print("error!")
				exit(1)
			type_var = 1;
			continue
		if type_var == 1:
			row = row.split('\t')
			freqs.append(row)
			type_var = 2;
			continue
		if type_var == 2:
			row = row.split('\t')
			doms.append(row)
			total += len(row)
			seq2length[names[len(names) - 1]] = len(row)
			type_var = 0
			continue
	return (names, freqs, doms, seq2length, total)

def read_test_dataset(filename, kmer_size): 
	names = []
	freqs = []
	seq2length = {}
	total = 0
	print(filename)
	with (open(filename)) as datafile:
		lines = datafile.readlines()
	type_var = 0
	for row in lines:
		if row[0] == '>':
			names.append(row[1:-1])
			if type_var != 0:
				print("error!")
				exit(1)
			type_var = 1;
			continue
		if type_var == 1:
			row = row.split('\t')
			freqs.append(row)
			total += (len(row) + kmer_size - 1)
			type_var = 0;
			continue
	return (names, freqs, seq2length, total)

def pick_test_sample(test_size, total_size, seq_names):
	doms = []
	freqs = []
	dom2freq = {}
	with open("/home/armen.abnousi/olldomain/datasets/pfam_doms_histogram.txt") as dom_file:
		dom_lines = dom_file.readlines()
	for line in dom_lines:
		name = (line.split('\t'))[0]
		freq = (line.split('\t'))[1]
		if int(float(freq)) > 100:
			doms.append(name)
			dom2freq[name] = freq
	test_size = int(test_size * len(seq_names))
	indices = sorted(random.sample(range(total_size), test_size))
	test_seq_names = [seq_names[i] for i in indices]
	return test_seq_names

def pick_test_sample_by_dom(test_size, total_size, seq_names):
	doms = []
	freqs = []
	dom2freq = {}
	test_seq_names=[]
	with open("/home/armen.abnousi/olldomain/datasets/pfam_doms_histogram.txt") as dom_file:
		dom_lines = dom_file.readlines()
	for line in dom_lines:
		name = (line.split('\t'))[0]
		freq = (line.split('\t'))[1]
		if int(float(freq)) > 100:
			doms.append(name)
			dom2freq[name] = freq
	test_size = int(test_size * len(doms))
	indices = sorted(random.sample(range(len(doms)), test_size))
	rand_doms = [ doms[i] for i in indices]
	with open("/home/armen.abnousi/olldomain/datasets/seq_dom_mapping.txt") as seq_file:
		seq_lines = seq_file.readlines()
	for line in seq_lines:
		content = line.split('\t')
		seq_name = content[0]
		content = content [1 : ]
		for dom_name in content:
			if dom_name in rand_doms and seq_name in seq_names:
				test_seq_names.append(seq_name)
				break
	return test_seq_names
	      
def dom_count_for_test_seq(seq2length, test_seq_names):
	lines = []
	dom_lines = []
	seq2doms = {}
	seq2domcount = {}
	with open("/home/armen.abnousi/olldomain/datasets/pfam_doms_and_count.csv") as dom_file:
		dom_lines = dom_file.readlines()
	for line in dom_lines:
		content = line.split("\t")
		dom_seq_name = content[0]
		if len(content) > 1:
			seq2doms[dom_seq_name] = content[ 1 : ]
	del dom_lines	
	seq_names = []
	total_test_length = 0
	for seq_name in test_seq_names:
		total_test_length += seq2length[seq_name]
		locs = [0 for i in range(seq2length[seq_name])]
		doms = []
		if seq_name in seq2doms:
			doms = seq2doms[seq_name]
		for i in range(int(len(doms) / 3)):
			dom_start = int(float(doms[(i * 3)])) - 1
			dom_end = int(float(doms[(i * 3) + 1])) - 1
			dom_count = int(float(doms[(i * 3) + 2]))
			for loc in range(dom_start, dom_end + 1):
				if locs[loc] < dom_count:
					locs[loc] = dom_count
		seq2domcount[seq_name] = locs
	return seq2domcount, total_test_length

if(__name__ == '__main__'):
	main(sys.argv[1:])
