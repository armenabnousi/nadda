//this prints out the frequency distribution of each start position for each sequence
//#define KEY_LENGTH 4

#include "mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include "iostream"
#include "fstream"
#include "sys/stat.h"
#include "mapreduce.h"
#include "keyvalue.h"
#include <vector>
#include <math.h>
#include <algorithm>
#include <sys/time.h>
#include <unistd.h>
#include <ctime>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/iter_find.hpp>
using namespace std;
using namespace MAPREDUCE_NS;

int first_pos_sorter(char *value1, int len1, char *value2, int len2);
void freq_counter( char*, int, char *,int,int*, KeyValue*, void*);
void mapper_chars(int, char *, int, KeyValue *, void *);
void profile_gen(char* key, int keybytes, char* values, int nvalues, int* valuebytes, KeyValue *kv, void* ptr);

int kmerlen;
/* ---------------------------------------------------------------------- */

int main(int narg, char **args)
{
  if (narg!=7)
  {
    printf("ERROR: Usage mrsort (number of maps) (page size) (kmer length) (separation delta) (input file name) (output_filename)");
    return(1);
  }
  MPI_Init(&narg,&args);
  MapReduce *mr = new MapReduce(MPI_COMM_WORLD);
  int nmap=atoi(args[1]);
  mr -> memsize = atoi(args[2]);
  kmerlen=atoi(args[3]);
  int sepdelta=atoi(args[4]);
  string input_fname(args[5]);
  string output_filename(args[6]);
  mr->verbosity = 0;
  mr->timer = 0;
  mr->outofcore=0;
  
  //Creating a char* contatining the dir for files including sequences
  char** dirname=new char*[1];
  dirname[0]=new char[input_fname.length() + 1];
  strcpy(dirname[0], input_fname.c_str());

  //Calling MapReduce functions
  int nwords = mr->map(nmap,1,dirname,0,0,"\n>",sepdelta,mapper_chars,NULL);
  int nunique = mr->collate(NULL);
  mr->sort_multivalues(5);
  mr->reduce(freq_counter,NULL);	
  mr->collate(NULL);
  mr->sort_multivalues(first_pos_sorter);
  MPI_Barrier(MPI_COMM_WORLD);
  nunique=mr->reduce(profile_gen,NULL);
  MPI_Barrier(MPI_COMM_WORLD);

  //printing the result to a file
  char file[output_filename.length() + 1];
  strcpy(file, output_filename.c_str());
  mr->print(file,0,-1,1,5,5);

  //double tstop = MPI_Wtime();   
  delete mr;
  delete[] dirname[0];
  delete[] dirname;
 
  MPI_Finalize();
  return(0);
}

/* ----------------------------------------------------------------------
   read a file
   for each word in file, emit key = word, value = NULL
------------------------------------------------------------------------- */

void mapper_chars(int itask, char *text, int size, KeyValue *kv, void *ptr)
{
	std::string nameBeginSep = ">";
	std::string nameEndSep = "\n";
	std::string s(text);
    	std::list<std::string> seqList;
    	boost::iter_split(seqList, s, boost::first_finder("\n>"));
	if( seqList.front().compare("") == 0 ) 
	{
		seqList.pop_front();
	}
	BOOST_FOREACH(std::string fastaSeq, seqList)
    	{    
		std::list<std::string> seqLines;
		boost::iter_split(seqLines, fastaSeq, boost::first_finder("\n"));
		std::string nameLine = seqLines.front();
		nameLine = nameLine.substr(nameLine.find(nameBeginSep) + 1);
		nameLine = nameLine.substr(0, nameLine.find(nameEndSep));
		//cout<< "name is: " << nameLine << '\n';
		char seqName[nameLine.length() + 1];
                strcpy(seqName, nameLine.c_str());
		seqLines.pop_front();
		std::string sequence = "";
		BOOST_FOREACH(std::string seqLine, seqLines)
		{
			sequence += seqLine;
		}
		for(int i = 0; i <= sequence.length() - kmerlen; i++)
		{
			std::string kmer = sequence.substr(i, kmerlen);
			char kmerChar[kmer.length()+1];
			strcpy(kmerChar, kmer.c_str());
			char value[1 + nameLine.length() + 1 + sizeof(int)];
			value[0] = 'k';
			strcpy(value + 1, nameLine.c_str());
			memcpy(value + 1 + nameLine.length() + 1, (char*)(&i), sizeof(int));
			//int seqLen = sequence.length();
			//memcpy(value + 1 + nameLine.length() + 1 + sizeof(int), (char*)(&seqLen), sizeof(int));
			kv -> add(kmerChar, kmer.length()+1, value, 1 + nameLine.length() + 1 + sizeof(int));
		}
		sequence = "s" + sequence;
		char seq[sequence.length() + 1];
		strcpy(seq, sequence.c_str());
		kv -> add(seqName, nameLine.length() + 1, seq, sequence.length() + 1);
		//cout << "seq: " << seq << " from: " << seqName << '\n';
    	}
}

void freq_counter(char* key, int keybytes, char* values, int nvalues, int* valuebytes, KeyValue *kv, void* ptr)
{
	vector <string> seqs;
	vector < vector <int> > positions;
	if(values[0] == 's')
	{
		kv -> add(key, keybytes,values, valuebytes[0]);
	}
	else
	{
		int distinctValues = 1;
		std::string seq1(values + 1);
		int pos1 = *(int*)(values + 1 + seq1.length() + 1);
		seqs.push_back(seq1);
		vector <int> inSeqPositions;
		inSeqPositions.push_back(pos1);
		int seq2Offset = valuebytes[0];
		for(int i = 1; i < nvalues; i++)
		{
			std::string seq2(values + seq2Offset + 1);
			if(values[seq2Offset] != 'k') 
			{
				cout<<"WRONG111!\n";
			}
			int pos = *(int*)(values + seq2Offset + 1 + seq2.length() + 1);
			if( seq2.compare(seqs.back()) == 0)
			{
				inSeqPositions.push_back(pos);
			}			
			else
			{
				positions.push_back(inSeqPositions);
				inSeqPositions.clear();
				seqs.push_back(seq2);
				inSeqPositions.push_back(pos);
			}
			seq2Offset += valuebytes[i];
		}
		positions.push_back(inSeqPositions);
	}

	if(seqs.size() > 1)
	{
		int distinctValues = seqs.size();
		for(int i = seqs.size(); i > 0; i--)
		{ 
			std::string seq = seqs.back();
			for(int j = (positions.back()).size(); j > 0; j--)
			{
				int pos = (positions.back()).back();
				char key[seq.length() + 1];
				strcpy(key, seq.c_str());
				char value[1 + sizeof(int) * 2];
				value[0] = 'f';
				memcpy(value + 1, (char*)(&pos), sizeof(int));
				memcpy(value + 1 + sizeof(int), (char*)(&distinctValues), sizeof(int));
				//cout<<"sendingto "<<key<<" : "<<pos<<" "<<distinctValues<<'\n';
				kv -> add(key, seq.length() + 1, value, 1 + sizeof(int) * 2);
				(positions.back()).pop_back();
			}
			positions.pop_back();
			seqs.pop_back();
		}
	}
}

void profile_gen(char* key, int keybytes, char* values, int nvalues, int* valuebytes, KeyValue *kv, void* ptr)
{
	int offset=0;
	vector<vector<int> > freq;
	std::string sequence;
	bool noseq=true;
	for(int i=0;i<nvalues;i++)
	{
		if(values[offset]!='s')
		{
			if(values[offset] != 'f')cout<<"WRONG222!\n";
			int current=*(int*)(values+offset + 1);
			int counter=*(int*)(values+offset + 1 +sizeof(int));
			char value[20];
        	        int size=sprintf(value,"%d %d",current,counter);
        	        vector<int> freqi(2);
			//cout<<"a:"<<counter<<'\n';
        	        freqi[0]=current;
        	        freqi[1]=counter;
        	        freq.push_back(freqi);
		}
		else
		{
			noseq=false;
			//delete[] sequence;
			sequence = std::string(values + offset + 1);
			//sequence=new char[valuebytes[i]];
			//memcpy(sequence,values+offset,valuebytes[i]);
		}
		offset+=valuebytes[i];
	}
	if(noseq)cout<<"WRONG333!\nhere: "<<key<<" no seq found\n";
	string key_str = '>' + string(key);
	string value_str = "";
	int j = 0;
	if (freq.size() == 0)
	{
		value_str += "1";
		int i = 1;
		while (i < sequence.length() - kmerlen + 1)
        	{
			value_str += ("\t1");
                	i++;
        	}
	}
	else
	{
		if (freq[0][0] == 0)
		{
			value_str += std::to_string((long long int) freq[0][1]);
			j++;
		}
		else
		{
			value_str += "1";
		}
		int i=1;
	        for(j;j<freq.size();j++)
	        {
			if (i >= sequence.length())
			{
				cout<<"here is the mistake"<< i <<" "<<j<<" "<<key<<" "<<sequence.length()<<'\n';
				break;
			}
	                char value[20];
	                if (i==freq[j][0])
	                {
				value_str += ('\t' + std::to_string((long long int) freq[j][1]));
	                        i++;
	                }
	                else
	                {
	                        value_str += "\t1";
				j--;
				i++;
	                }
	        }
		while (i < sequence.length() - kmerlen + 1)
		{
			value_str += "\t1";
			i++;
		}
	}
	char new_key[key_str.length() + 1];
	strcpy(new_key, key_str.c_str());
	char new_val[value_str.length() + 1];
	strcpy(new_val, value_str.c_str());
	kv -> add(new_key, key_str.length() + 1, new_val, value_str.length() + 1);
}

int first_pos_sorter(char *value1, int len1, char *value2, int len2)
{
        if((*(int*)(value1))<(*(int*)(value2)))
                return(-1);
        else
                return(1);
}
