#!/bin/bash
#PBS -k o
#PBS -l nodes=4:ppn=8,walltime=1:00:00
#PBS -m abe
#PBS -N zc_test_dataset_gen
#PBS -j oe

module load openmpi/1.8.6-gcc
cd ~/nadda/
########################################################################################
#SET THESE PARAMETERS:
threshold=50
input_fasta=
output_dataset_filename=
kmer_size=6
num_process=32 #number of processors
sep_delta=30000 #make sure is larger than the length of the longest sequence

########################################################################################
#Optional parameters:
page_size=4096
num_maps=1000

########################################################################################
#GENERATING A DATASET INCLUDING THE FREQUENCIES OF KMERS
mpiexec -np $num_process ./profiler $num_maps $page_size $kmer_size $sep_delta $input_fasta $output_dataset_filename"_temp"
cut -d ' ' -f 9- $output_dataset_filename"_temp" > $output_dataset_filename
sed -i 's/, value /\n/g' $output_dataset_filename
rm $freq_dataset"_temp"
