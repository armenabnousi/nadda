#!/bin/bash
#PBS -k o
#PBS -l nodes=4:ppn=8,walltime=1:00:00
#PBS -m abe
#PBS -N dataset_gen
#PBS -j oe

module load openmpi/1.8.6-gcc
cd ~/nadda/
########################################################################################
#SET THESE PARAMETERS:
threshold=50
input_fasta=
pfammed_filename=
output_dataset_filename=
kmer_size=6
num_process=32 #number of processors
sep_delta=30000 #make sure is larger than the length of the longest sequence

########################################################################################
#Optional parameters:
pfam_tabular_file=$pfammed_filename"_tabular"
freq_dataset=$input_fasta"_freqs"
page_size=4096
num_maps=1000

########################################################################################
#GENERATING A DATASET INCLUDING THE FREQUENCIES OF KMERS
mpiexec -np $num_process ./profiler $num_maps $page_size $kmer_size $sep_delta $input_fasta $freq_dataset"_temp"
cut -d ' ' -f 9- $freq_dataset"_temp" > $freq_dataset
sed -i 's/, value /\n/g' $freq_dataset
rm $freq_dataset"_temp"

#######################################################################################
#GENERATING FINAL DATASETS REQUIRED BY nadda.py (APPENDING THE PFAM ANNOTATED REGIONS TO FREQUENCIES)
sed -e '1,28d' < $pfammed_filename > $pfam_tabular_file
sed -i -r 's/[ ]+/\t/g' $pfam_tabular_file
python ./freq_dataset_gen_from_pfam.py $freq_dataset $pfam_tabular_file $output_dataset_filename $threshold $kmer_size
